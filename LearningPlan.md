## Abstract 

This document contains an overview of why it is import to use the correct software management techniques when working with large software projects and the advantages of doing this using Gantt charts and Program Breakdown Structure. This is trying to solve the problem of managing multiple concurrent tasks in the most efficient way possible and provide a high-level overview of the entire project. Without this it will be difficult to have popper time monument and some tasks may end up being missed or not having enough time allocated to complete them to the expectations.  The result of this task was 2 different graphs that provide different views of the entire course. One proving a top down abstract view of the entire coerce and the other containing all of the same information in a more detailed graph that helps to show how the time elements of each piece of work link together and how some aspects are dependent on each other.  

## Introduction 

When working with large software projects or any large pieces of work it can be difficult to manage time correctly to allow enough time for each aspect of the project. Creating an outline of the task in the form of a Gantt chart can help to manage time more efficiently as each aspect of the project is broken up into more manageable tasks and it allows you to see a structured order of each task. This is especially helpful when working in a project with a large group of people as it can help separate tasks between different people allowing them to collaborate better between individual tasks. 

For a complex task like a university degree where it is made up of 3 separate years with multiple modules making up each year then 120 credits worth of modules for each of these. Having a product break down structure (PBS) gives a visual representation of the entire project broken up into smaller tasks that can allow you to manage time more effectively for each module. The purpose of this task is to create an abstract overview of the entire degree to help with planning work at specific times, this helps with things like revising for exams and making sure coursework assignments are completed on time. 

This document focuses on the advantages and disadvantages of having proper project management and how it can affect the fine project with reference to time management and planning. It also discusses the process of creating a Gantt chart and a PBS using tools that allow you to crate complex and detailed diagrams easily. 

## Background 

In J.C. van Tonder’s (1) talks about how a product breakdown structure defines a project into activities that give a tangible result. This is breaking down each section of the project, or in this case a computer science degree. An example of this is an online test that is part of a module whereupon coemption you receive a grade from the piece of work that give you feedback on how well the task has gone. This is also helpful in the way that it allows you to track your progress throughout each element of the module or year.  

In Sommerville’s (2) paper about project management, he talks about the most important aspects of having good project management, which he breaks down into 4 main components. The speed of delivery of the project, maintaining the original budget, maintaining a functional team and delivering to expectations. The speed of delivery of the project is especially important in this case as in a university degree because you have strict deadlines that work needs to be completed before in order to get the best possible marks. This fits well into the last point of finishing a project to expectations since if projects aren’t completed within expected time in the case of a degree where deadlines for tasks can’t be extended you will not be able to complete the project to the original expectations if you are not able to keep track of time management.  

The use of Gantt charts is another important aspect of project management as it can help with prioritising each individual task since they can be given weights for priority to help you determine which task should be completed first and also how many hours each task will take to complete.   

In the Gantt chart, you can see for each day in the course which work is due on that day and the number of hours it will take to complete. This means you can use it to plan in advanced when work needs to be started for each assignment. 

 

## Reflection 

I produced 2 separate charts in the form of a Gantt chart and a product break down structure (PBS) to allow me to gain a visual representation of all the work contained within my course that can help with planning tasks and revision later in the year. Producing these also helped me to gain a better understanding of how to use these programs, in this case I used a program called MindView7 that allows you to add detailed entries for each subsection of the course. Taking into account things like duration for each task and the number of hours of work within this time that each task will take to do. It also allows you to clearly show the decencies between different tasks like how doing Java as a second-year module is dependent on completing the programming module in the first year. 

The main advantage I found with using a Gantt chart is that they provide a very good visual representation that helps to give you a high-level overview of all the objectives needed to complete a task. This makes each individual task look more manageable.  The only disadvantage I was able to find when creating this Gantt chart was the complexity of the chart as a whole when adding each of the modules for a 3-year project. In the Gantt chart I created it spanned across multiple A4 sheets which can become overwhelming.  

This was the same when creating the top down view of this in the form of a PBS when looking at the project as a whole it became over complicated and needs to be read in a broken-down form like for each year to make it more readable.  

Another disadvantage I found was with the timing of each module as for some assignments the timing may change which wouldn’t be reflected in the chart or would have to be continuously updated to prevent the timings being inaccurate, another issue with timing is it can be difficult to represent in the chart how many hours each piece of work will require and for exams to include the time that is required for preparation and revision for the exam. This is because the chart represents the hours to complete each task but does not represent well the preparation that goes along with doing each task.  

When creating the chart using the MindView tool it was easy to add each element of the tool into a structured chart since it gave you a graphical representation of what you were doing in a top-down view. All you needed to was keep going through the structure of tasks adding each one, it allowed you to add as much detail for each task that was necessary. Another advantage of MindView is it was able to use the same values in the top down product breakdown structure to create the Gantt chart as it had the same values in the same hierarchy, all I needed to do was add the time values into the chart and the number of hours each of these tasks represented. 

Another program I was going to use was Visio which is a Microsoft tool used for creating vector graphics that could create a PBS and Gantt chart, the reason I didn’t use it is because it is a less specialised tool for project management compared to MindView. This means it would have taken longer to produce the same result. 

 

 

## References  

(1)  https://c.ymcdn.com/sites/www.projectmanagement.org.za/resource/resmgr/conference_proceedings_2002/10.pdf - J.C. van Tonder and M.C. Bekker  

(2) https://web.stanford.edu/group/CIFE/cgi-bin/drupal/sites/default/files/WP115_0.pdf - Forest Peterson, Renate Fruchter & Martin Fischer

## product breakdown structure

![product breakdown structure](https://csgitlab.reading.ac.uk/nh006220/Coursework2/raw/master/Images/DegreeFinal.jpg) 

## Gantt chart 

![Gantt Chart](https://csgitlab.reading.ac.uk/nh006220/Coursework2/raw/master/Images/DegreeGanttFinal%20.jpg)

## CV

![Click Me for Word file](https://csgitlab.reading.ac.uk/nh006220/Coursework2/raw/master/CV-Sam.docx)

## CV in .md

| 173 Binderton, Chichester, West Sussex, PO18 0JS |
| --- |
| Mobile: 07711 233 287 |
| Email: Samuel.pink@outlook.com |
## Samuel Pink
 

# Objective
  Ultimate goal to work for a blue chip company within the computer industry whilst still pursuing my bike racing at a good level.
# Experience


# 2007 - 2017
 From the age of 12 I started taking part in triathlons, and very quickly became very successful at these, and within a few years I was an elite triathlete for the country.  Therefore I was fitting in my triathlon training and racing commitments around my education, so I had very little time to work as well.  However, in the summer holidays I did some leaflet distribution for Yellow Pages and Ikea, working very hard delivering 9000 catalogues in just 3 weeks, together with dog walking to earn a little extra money to help towards the cost of my sports equipment.  I am very hard working, as will be seen by the fact that I have been 100% committed to triathlon with my highest ranking of 10th
 in the UK at the U20s Championships.
# June 2016
 I worked at the Goodwood Festival of Speed working very long hours helping serving and clearing away, and I really enjoyed working on my own initiative and being part of a team.

# July 2016 – September 2018
 As part of my 6th form I undertook a week&#39;s work experience at a company called GSA (Global Student Accommodation).  I was put forward for this by my IT teacher as he was asked to find someone who was passionate about computers.  During this week I carried out numerous assignments on my own, such as formatting computers and writing a programme and as a result of my achievements I was offered a summer job there for the whole summer holidays. At the end of the summer break, as I had decided to take a gap year before starting at university, I was offered a full time job with GSA.  I primarily carried out programming work and undertook various projects, such as ……… I started at Reading University to study Computer Science in September 2018 but continue to do some work for GSA on a remote basis.   
# July 2016 - September 2018
 I started working full time at GSA (Global Student Accommodation) as a software engineer working office 365 integration and other company products, writing software in PowerShell, C#, SQL and HTML.

# Education
 
Up until my A levels I was home schooled, to enable me to focus on my triathlon training.  I took 5 iGCSEs and achieved the following results: Physics – A Computer Studies – A Biology - B Maths – B English – C
I then returned to Midhurst Rother College to study for my A levels and achieved the following results: 
Computer Science – distinction 
PE  - A 
IT – BTEC distinction*

# Interests
 My main interest is road biking and also swimming and running.  I was an elite triathlete for 5 years.  I am not currently competing but go out on regular long bike rides and also continue with my swimming and occasional running.  My main focus currently is my studies, but I wish to return to competitive biking in the near future.

# References
 Robert Bertora
Global Student Accommodation 



 