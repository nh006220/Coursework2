| 173 Binderton, Chichester, West Sussex, PO18 0JS |
| --- |
| Mobile: 07711 233 287 |
| Email: Samuel.pink@outlook.com |
## Samuel Pink
 

# Objective
  Ultimate goal to work for a blue chip company within the computer industry whilst still pursuing my bike racing at a good level.
# Experience


# 2007 - 2017
 From the age of 12 I started taking part in triathlons, and very quickly became very successful at these, and within a few years I was an elite triathlete for the country.  Therefore I was fitting in my triathlon training and racing commitments around my education, so I had very little time to work as well.  However, in the summer holidays I did some leaflet distribution for Yellow Pages and Ikea, working very hard delivering 9000 catalogues in just 3 weeks, together with dog walking to earn a little extra money to help towards the cost of my sports equipment.  I am very hard working, as will be seen by the fact that I have been 100% committed to triathlon with my highest ranking of 10th
 in the UK at the U20s Championships.
# June 2016
 I worked at the Goodwood Festival of Speed working very long hours helping serving and clearing away, and I really enjoyed working on my own initiative and being part of a team.

# July 2016 – September 2018
 As part of my 6th form I undertook a week&#39;s work experience at a company called GSA (Global Student Accommodation).  I was put forward for this by my IT teacher as he was asked to find someone who was passionate about computers.  During this week I carried out numerous assignments on my own, such as formatting computers and writing a programme and as a result of my achievements I was offered a summer job there for the whole summer holidays. At the end of the summer break, as I had decided to take a gap year before starting at university, I was offered a full time job with GSA.  I primarily carried out programming work and undertook various projects, such as ……… I started at Reading University to study Computer Science in September 2018 but continue to do some work for GSA on a remote basis.   
# July 2016 - September 2018
 I started working full time at GSA (Global Student Accommodation) as a software engineer working office 365 integration and other company products, writing software in PowerShell, C#, SQL and HTML.

# Education
 
Up until my A levels I was home schooled, to enable me to focus on my triathlon training.  I took 5 iGCSEs and achieved the following results: Physics – A Computer Studies – A Biology - B Maths – B English – C
I then returned to Midhurst Rother College to study for my A levels and achieved the following results: 
Computer Science – distinction 
PE  - A 
IT – BTEC distinction*

# Interests
 My main interest is road biking and also swimming and running.  I was an elite triathlete for 5 years.  I am not currently competing but go out on regular long bike rides and also continue with my swimming and occasional running.  My main focus currently is my studies, but I wish to return to competitive biking in the near future.

# References
 Robert Bertora
Global Student Accommodation 